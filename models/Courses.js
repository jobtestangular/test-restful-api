var db = require('../dbconnection');

module.exports = {
  getAllCourses,
  getallcategorys,
  getalldays,
  addCourse,
  updateCourse,
  deleteCourse,
  checkCourse,
  searchCourse,
  checCoursekName,
  checkCourseID
};

async function authenticate(user, callback) {
  return db.query("SELECT * FROM memberships WHERE username=? AND password=?", [user.username, user.password], callback);
}

async function getAllCourses(callback) {
  return db.query("SELECT * FROM view_courses", callback);
}

async function getallcategorys(callback) {
  return db.query("SELECT * FROM courses_category", callback);
}

async function getalldays(callback) {
  return db.query("SELECT * FROM courses_dayofweek", callback);
}

async function getalltype(callback) {
  return db.query("SELECT * FROM membership_type", callback);
}

async function getPrenameActive(callback) {
  return db.query("SELECT * FROM `prename` WHERE `pren_status` = 'Active'", callback);
}

async function getPrenameOther(callback) {
  return db.query("SELECT * FROM `prename` WHERE `pren_status` <> 'Active'", callback);
}

async function getUserByUsername(username, callback) {
  return db.query("SELECT username FROM memberships WHERE username=?", [username], callback);
}

async function addCourse(courseData, callback) {
  return db.query("INSERT INTO courses set ?", courseData, callback);
}

async function checkCourse(courseData, callback) {
  return db.query("SELECT * FROM `view_courses` WHERE (`starttime`>= ? AND `endtime` <= ?) OR (`starttime`<= ? AND `endtime` >= ?)", [courseData.starttime, courseData.endtime, courseData.starttime, courseData.endtime], callback);
}

async function checkCourseID(courseID, callback) {
  return db.query("SELECT * FROM `view_courses` WHERE courses_id = ?", [courseID], callback);
}

async function checCoursekName(courseData, callback) {
  return db.query("SELECT * FROM `view_courses` WHERE name = ?", [courseData.name], callback);
}

async function searchCourse(courseData, callback) {
  let sqlCon = [];
  if (courseData.name != '') {
    sqlCon.push("name LIKE '%" + courseData.name + "%'");
  }
  if (courseData.starttime != '' && courseData.endtime != '') {
    sqlCon.push("(`starttime`= '" + courseData.starttime + ":00' AND `endtime` = '" + courseData.endtime + ":00')")
  }
 

  let sqlStr = "1";
  if (sqlCon.length > 0) {
    sqlStr = sqlCon.join(' AND ');
  } else {
    sqlStr = 1;
  }
  return db.query("SELECT * FROM `view_courses` WHERE "+sqlStr, callback);
}

async function updateCourse(courseID, courseData, callback) {
  return db.query("UPDATE courses SET ? WHERE courses_id=?", [courseData, courseID], callback);
}
async function deleteCourse(courseID, callback) {
  return db.query("DELETE FROM courses WHERE courses_id=?", courseID, callback);
}