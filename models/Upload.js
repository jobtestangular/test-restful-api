var fs = require("fs");
var path = require('path');
var async = require('async');
var cmd = require('node-cmd');

var rootPath = "./asset/";

exports.fileupload = function (req, res) {

  fs.readFile(req.files[0].path, (err, data) => {
    if (err) {
      console.log("err ocurred", err);
    }
    else {
      var writePath = rootPath + req.body.path + '/' + req.body.filename;
      fs.writeFile(writePath, data, (err) => {
        if (err) {
          console.log("error occured", err);
        }
        else {
          cmd.run('rm -rf ' + rootPath + req.files[0].filename);
          res.send({
            status_code: "200",
            path: rootPath,
            filename: req.body.filename
          });
        }
      });
    }
  });

}

