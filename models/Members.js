var db = require('../dbconnection');

module.exports = {
  getAllUser,
  addUser,
  updateUser,
  deleteUser,
  getPrenameActive,
  getUserByUsername,
  getPrenameOther,
  getalltype,
  getAllGender,
  authenticate,
  getById

};

async function authenticate(user, callback) {
  return db.query("SELECT * FROM view_memberships WHERE username=? AND password=?", [user.username, user.password], callback);
}

async function getAllUser(callback) {
  return db.query("SELECT * FROM view_memberships", callback);
}

async function getAllGender(callback) {
  return db.query("SELECT * FROM membership_gender", callback);
}

async function getalltype(callback) {
  return db.query("SELECT * FROM membership_type", callback);
}

async function getPrenameActive(callback) {
  return db.query("SELECT * FROM `prename` WHERE `pren_status` = 'Active'", callback);
}

async function getPrenameOther(callback) {
  return db.query("SELECT * FROM `prename` WHERE `pren_status` <> 'Active'", callback);
}

async function getUserByUsername(username, callback){
  return db.query("SELECT username FROM memberships WHERE username=?", [username], callback);
}

async function getById(id, callback){
  return db.query("SELECT * FROM view_memberships WHERE id=?", [id], callback);
}

async function addUser(User, callback){
  return db.query("INSERT INTO memberships set ?", User, callback);
}

async function updateUser(username, User, callback){
  return db.query("UPDATE memberships SET ? WHERE id=?", [User, username], callback);
}
async function deleteUser(id, callback){
  return db.query("DELETE FROM memberships WHERE id=?", id, callback);
}