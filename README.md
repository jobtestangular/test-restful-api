## Git Repository
[Git: Course Management System](https://gitlab.com/jobtestangular/test-restful-api.git)

Clone project from gitlab `git clone https://gitlab.com/jobtestangular/test-restful-api.git`

> Project Directory **RUN** `cd test-restful-api`

## 1. Config Database

> EDIT **dbconnection.js**

<pre>
var mysql = require('mysql');
var connection = mysql.createPool({
  host: 'db', 
  user: 'root', 
  password: 'test', 
  database: 'test-resful-api'

});
module.exports = connection;
</pre>

## 2. Phpmyadmin Mysql Databases
### 1. Build Docker by docker-compose for buid database and Phpmyadmin

> Build **docker-compose.yml**

<pre>version: "2"
services:
  db:
    image: mariadb
    environment:
      - MYSQL_ROOT_PASSWORD=test
      - MYSQL_DATABASE=radio
    volumes:
      - /database:/var/lib/mysql
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: phpmyadmin
    environment:
      - PMA_ARBITRARY=1
      - PMA_HOST=db
    restart: always
    ports:
      - 8080:80
    volumes:
      - /sessions
    links:
      - db
</pre>

> **RUN** `docker-compose up -d --build`

### 2. Check Docker network

> **RUN** `docker network ls`

<pre>
PS C:\Users\ARMSTRONG\test-restful-api> <b><red>docker network ls<red></b>
NETWORK ID          NAME                       DRIVER              SCOPE
85005c0a062e        bridge                     bridge              local
e3d457e4eca8        host                       host                local
e4085b222b7c        none                       null                local
c93548a903b8        <b>test-restful-api_default</b>   bridge              local
</pre>

### 3. Import Database

> File **test-resful-api.sql**

1. goto phpmyadmin page [http://localhost:8080](http://localhost:8080)
2. login host : **db** user: **root** password: **test** 
3. create database name **test-resful-api**
4. import from **test-resful-api.sql**


> USE Docker network **test-restful-api_default**

## 3. NodeJS RestFUL APIs 
### 1. Create Docker image

> **RUN** `docker build . -t test-restful-api` from **Dockerfile**

### 2. Run Docker container

> **RUN** `docker run -e VERSION=1.1 --name course_management_system_apis -d -p 3000:3000 --restart=always test-restful-api`


### 3. Add NodeJS RestFUL APIs to Docker Network 
> **Network** use docker network same 2. Phpmyadmin Mysql Databases

> **RUN** `docker network connect test-restful-api_default course_management_system_apis`

### 4. Show docker network

> **RUN** `docker network inspect test-restful-api_default`

<pre>
PS C:\Users\ARMSTRONG\test-restful-api> <b>docker network inspect test-restful-api_default</b>
[
    {
        "Name": "test-restful-api_default",
        "Id": "c93548a903b86b8a9d90befdeea4f7cbeeda93016b97e091025b8a76171c9a39",
        "Created": "2019-04-15T12:04:24.3497654Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": true,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "18a4db019b7144d2a5f503abf46e730c65629d20a5f7b0018eda5c7bb7043ec5": {
                "Name": <b>"course_management_system_apis",</b> - Check OK
                "EndpointID": "74b9bfefb0a59e2b0ae96342a4c68611666d66511c63c2f740d63de27e39ac97",
                "MacAddress": "02:42:ac:12:00:04",
                "IPv4Address": "172.18.0.4/16",
                "IPv6Address": ""
            },
            "5e4bcdb955f397683153321dec3e93638c38bdf6e13e39fefad3f8ba9c7aa669": {
                "Name": "phpmyadmin",
                "EndpointID": "fdb423381efebbdd6e576de2ccc3ff340d43f26e6db60a918feeaf604280b6a8",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            },
            "e1c72eab363ebfc8791f42f3bcf81568cdd77c0a5543d0153a0049d8043ad6df": {
                "Name": "test-restful-api_db_1",
                "EndpointID": "579318f582535874c92386e54fb5a1d901ee6cdd26d57d821b0f94a7697e104e",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {
            "com.docker.compose.network": "default",
            "com.docker.compose.project": "test-restful-api",
            "com.docker.compose.version": "1.23.2"
        }
    }
]
</pre>

## 5. Local Run Project

1. install **nodemon** run `npm install nodemon -g`
2. install **package** run `npm install` file **package.json**
3. in directory `test-restful-api` **RUN** `nodemon` **or** **RUN** `npm start`

### 6. API URL

> **Example** [http://localhost:3000/courses](http://localhost:3000/courses)