-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Apr 15, 2019 at 01:32 PM
-- Server version: 10.3.14-MariaDB-1:10.3.14+maria~bionic
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test-resful-api`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `courses_id` varchar(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(250) NOT NULL,
  `category` int(10) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `number_student` int(5) NOT NULL,
  `number_student_registered` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='courses';

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`courses_id`, `name`, `description`, `category`, `subject`, `starttime`, `endtime`, `number_student`, `number_student_registered`) VALUES
('0001018', 'Preparatory English', 'ภาษาอังกฤษเพื่อเตรียมความพร้อม', 1, 'conversation group', '08:00:00', '09:00:00', 35, 0),
('1300102', 'Introduction to International Relations', 'ความสัมพันธ์ระหว่างประเทศเบื้องต้น', 2, 'ASIA', '09:00:00', '11:00:00', 50, 0),
('1301423', 'Theory of Comparative Political Economy', 'ทฤษฎีเศรษฐศาสตร์การเมืองเปรียบเทียบ', 8, 'Theory', '11:00:00', '15:00:00', 50, 0),
('0201100', 'Mathematics for Science 1', 'คณิตศาสตร์สำหรับวิทยาศาสตร์ 1\nสังกัด	คณะวิทยาศาสตร์, ภาควิชาคณิตศาสตร์\nหน่วยกิต	4 (4-0-8)\nสถานะรายวิชา:	ใช้งาน', 8, 'Mathematics for Science 1', '19:00:00', '20:40:00', 250, 0),
('1201108', 'Database Design', 'การออกแบบฐานข้อมูล\nสังกัด	คณะวิทยาการสารสนเทศ, สาขาระบบสารสนเทศเพื่อการจัดการ\nหน่วยกิต	3 (2-2-5)\nสถานะรายวิชา:	ใช้งาน', 4, 'Database Design', '08:00:00', '12:00:00', 50, 0),
('1201306', 'Software Engineering', '\nวิศวกรรมซอฟต์แวร์\nสังกัด	คณะวิทยาการสารสนเทศ, สำนักงานเลขา คณะสารสนเทศ\nหน่วยกิต	3 (2-2-5)\nสถานะรายวิชา:	ใช้งาน\nเงื่อนไขรายวิชา:	1201208', 4, 'Software Engineering', '13:00:00', '17:00:00', 45, 0),
('0201101', 'Mathematics for Science 2', 'คณิตศาสตร์สำหรับวิทยาศาสตร์ 2\nสังกัด	คณะวิทยาศาสตร์, ภาควิชาฟิสิกส์\nหน่วยกิต	4 (4-0-8)\nสถานะรายวิชา:	ใช้งาน\nเงื่อนไขรายวิชา:	0201100 และ CON0204222 หรือ\n0201100 หรือ\n0201100 หรือ\n0201100 หรือ\n0201100 หรือ\n0201100 หรือ\n0201100 หรือ\n0201100 หรือ\n020110', 8, 'Mathematics for Science 2', '17:00:00', '21:00:00', 70, 0),
('0104022', 'History of Asian', '\nประวัติศาสตร์เอเชีย\nสังกัด	คณะมนุษยศาสตร์และสังคมศาสตร์, สำนักงานเลขา คณะมนุษย์\nหน่วยกิต	3 (3-0-6)\nสถานะรายวิชา:	ใช้งาน', 3, 'History of Asian', '08:00:00', '11:00:00', 65, 0),
('0401209', 'Fundamental Nursing Practice', 'ปฏิบัติการพยาบาลรากฐาน\nสังกัด	คณะพยาบาลศาสตร์, สำนักงานเลขา คณะพยาบาล\nหน่วยกิต	2 (0-8-0)\nสถานะรายวิชา:	ใช้งาน\nเงื่อนไขรายวิชา:	0401206 และ 0401208 และ 0702251', 6, 'Nursing Practice', '08:00:00', '09:00:00', 8, 0),
('1001104', 'Tourist Behavior and Cross Cultural Communication', 'พฤติกรรมนักท่องเที่ยวและการสื่อสารข้ามวัฒนธรรม\nสังกัด	คณะการท่องเที่ยวและการโรงแรม, สำนักงานเลขา คณะการท่องเที่ยวและการโรงแรม\nหน่วยกิต	3 (3-0-6)\nสถานะรายวิชา:	ใช้งาน', 1, 'Tourist Behavior', '07:00:00', '08:00:00', 59, 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses_category`
--

CREATE TABLE `courses_category` (
  `category_id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `name_th` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses_category`
--

INSERT INTO `courses_category` (`category_id`, `name`, `name_th`) VALUES
(1, 'Language and Communication', 'กลุ่มภาษาและการสื่อสาร'),
(2, 'Humanities', 'กลุ่มมนุษยศาสตร์'),
(3, 'Social Science', 'กลุ่มสังคมศาสตร์'),
(4, 'Information and Technology', 'กลุ่มเทคโนโลยีสารสนเทศ'),
(5, 'Physical Education', 'กลุมพละศึกษา'),
(6, 'Health Science', 'กลุ่มวิทยาศาสตร์สุขภาพ'),
(7, 'Interdisciplinary', 'กลุ่มสหศาสตร์'),
(8, 'Science-Mathematics', 'กลุ่มวิทยาศาสตร์-คณิตศาสตร์');

-- --------------------------------------------------------

--
-- Table structure for table `courses_dayofweek`
--

CREATE TABLE `courses_dayofweek` (
  `day_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_th` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses_dayofweek`
--

INSERT INTO `courses_dayofweek` (`day_id`, `name`, `name_th`) VALUES
(1, 'Sunday', 'วันอาทิตย์'),
(2, 'Monday', 'วันจันทร์'),
(3, 'Tuesday', 'วันอังคาร'),
(4, 'Wednesday', 'วันพุธ'),
(5, 'Thursday', 'วันพฤหัสบดี'),
(6, 'Friday', 'วันศุกร์'),
(7, 'Saturday', 'วันเสาร์');

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

CREATE TABLE `memberships` (
  `id` bigint(20) NOT NULL COMMENT 'user id',
  `username` varchar(250) NOT NULL COMMENT 'username',
  `password` varchar(64) NOT NULL COMMENT 'password (MD5)',
  `prename` int(5) NOT NULL COMMENT 'prefix name of member',
  `firstname` varchar(255) NOT NULL COMMENT 'firstname of member',
  `lastname` varchar(255) NOT NULL COMMENT 'lastname of member',
  `nickname` varchar(50) NOT NULL,
  `gender` int(5) NOT NULL COMMENT 'gender (1 male, 2 female)',
  `birthday` date NOT NULL COMMENT 'date of birth',
  `mobile` varchar(255) DEFAULT NULL COMMENT 'เบอร์โทรศัพท์ (มือถือ)',
  `email` varchar(255) NOT NULL COMMENT 'ที่อยู่อีเมล',
  `type` int(5) NOT NULL COMMENT 'access permissions (1:Student, 2:Instructor, 3: Super Admin)',
  `image` varchar(250) DEFAULT NULL COMMENT 'profile pictrue',
  `facebook` varchar(150) DEFAULT NULL COMMENT 'facebook url',
  `line` varchar(150) DEFAULT NULL COMMENT 'Line ID',
  `view` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'view permission',
  `edit` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'edit permission',
  `del` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'delete permission',
  `token` varchar(50) NOT NULL COMMENT 'login troken',
  `insert_user_id` varchar(250) DEFAULT NULL COMMENT 'username of member is create this member',
  `insert_datetime` datetime DEFAULT NULL COMMENT 'date time of insert member',
  `update_user_id` varchar(250) DEFAULT NULL COMMENT 'username of member is edit member profile',
  `update_datetime` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'date time of edit member profile'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ตารางสิทธิ์การเข้าใช้งาน (บัญชีผู้ใช้งาน)' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `memberships`
--

INSERT INTO `memberships` (`id`, `username`, `password`, `prename`, `firstname`, `lastname`, `nickname`, `gender`, `birthday`, `mobile`, `email`, `type`, `image`, `facebook`, `line`, `view`, `edit`, `del`, `token`, `insert_user_id`, `insert_datetime`, `update_user_id`, `update_datetime`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 3, 'Jaroon', 'Khamket', 'arm', 1, '1991-06-04', '0925127771', 'jaroon.khamket@gmail.com', 3, 'http://localhost:3000/images/members/profiles/user-512.png', 'jaroon.khamket', '0925127771', 1, 1, 1, '21232f297a57a5a743894a0e4a801fc3', '', '2019-04-12 18:16:23', 'admin', '2019-04-14 10:09:46'),
(2, 'student01', 'e10adc3949ba59abbe56e057f20f883e', 4, 'Nancy', 'Talana', 'nan', 2, '2019-04-11', '', 'nan@gmail.com', 1, 'http://localhost:3000/images/members/profiles/user-512.png', '', '', 1, 0, 0, 'b26b953175d9cf65cfdc2bf6c5077404', '', '2019-04-14 05:04:25', 'student01', '2019-04-15 07:45:53'),
(3, 'student02', 'e10adc3949ba59abbe56e057f20f883e', 4, 'Kanokwan', 'Manorueng', 'NokNoi', 2, '1995-11-20', '', 'noknoi@gmail.com', 1, 'http://localhost:3000/images/members/profiles/Docker-logo-011.png', 'noknoi', 'noknoi', 1, 0, 0, '3836fb8e0d5844c27591c9f0b7c2ddfa', '', '2019-04-14 16:49:58', 'student02', '2019-04-14 09:54:44'),
(4, 'Instructor01', 'e10adc3949ba59abbe56e057f20f883e', 3, 'Jaroon', 'Khamket', 'arm', 1, '1991-06-04', '', 'jaroon.khamket@gmail.com', 2, 'http://localhost:3000/images/members/profiles/Student_Reading-512.png', '', '0925127771', 1, 0, 0, '5623dd1ef38fb98bf01e92157105418d', 'admin', '2019-04-14 17:07:21', '', '2019-04-14 10:05:43');

-- --------------------------------------------------------

--
-- Table structure for table `membership_gender`
--

CREATE TABLE `membership_gender` (
  `type_id` int(5) NOT NULL,
  `name` varchar(150) NOT NULL,
  `name_th` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_gender`
--

INSERT INTO `membership_gender` (`type_id`, `name`, `name_th`) VALUES
(1, 'Male', 'เพศชาย'),
(2, 'Female', 'เพศหญิง'),
(3, 'Unknown Gender', 'ไม่ทราบเพศ');

-- --------------------------------------------------------

--
-- Table structure for table `membership_type`
--

CREATE TABLE `membership_type` (
  `type_id` int(5) NOT NULL,
  `name` varchar(150) NOT NULL,
  `name_th` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_type`
--

INSERT INTO `membership_type` (`type_id`, `name`, `name_th`) VALUES
(1, 'Student', 'นักเรียน / นักศึกษา'),
(2, 'Instructor', 'อาจารย์ผู้สอน'),
(3, 'Administrator', 'ผู้ดูแลระบบ');

-- --------------------------------------------------------

--
-- Table structure for table `prename`
--

CREATE TABLE `prename` (
  `pren_id` int(5) NOT NULL COMMENT 'id',
  `pren_code` char(3) DEFAULT NULL COMMENT 'prifix name code',
  `prename_th` varchar(255) DEFAULT NULL COMMENT 'prefix name thai',
  `pren_status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive' COMMENT 'enable status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ตารางมาตรฐานรหัสข้อมูล (รหัสคำนำหน้านาม)' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `prename`
--

INSERT INTO `prename` (`pren_id`, `pren_code`, `prename_th`, `pren_status`) VALUES
(1, '001', 'เด็กชาย', 'Active'),
(2, '002', 'เด็กหญิง', 'Active'),
(3, '003', 'นาย', 'Active'),
(4, '004', 'นางสาว', 'Active'),
(5, '005', 'นาง', 'Active'),
(6, '006', 'นักโทษชายหม่อมหลวง', 'Inactive'),
(7, '007', 'นักโทษชาย', 'Inactive'),
(8, '008', 'นักโทษหญิง', 'Inactive'),
(9, '009', 'นักโทษจ่าสิบเอก', 'Inactive'),
(10, '010', 'นักโทษชายจ่าเอก', 'Inactive'),
(11, '011', 'นักโทษชายพลทหาร', 'Inactive'),
(12, '012', 'นักโทษชายร้อยตรี', 'Inactive'),
(13, '100', 'พระบาทสมเด็จพระเจ้าอยู่หัว', 'Inactive'),
(14, '101', 'สมเด็จพระนางเจ้า', 'Inactive'),
(15, '102', 'สมเด็จพระศรีนครินทราบรมราชชนนี', 'Inactive'),
(16, '103', 'พลโทสมเด็จพระบรมโอรสาธิราช', 'Inactive'),
(17, '104', 'พลตรีสมเด็จพระเทพรัตนราชสุด', 'Inactive'),
(18, '105', 'พระเจาวรวงศ์เธอพระองค์หญิง', 'Inactive'),
(19, '106', 'พระเจ้าวรวงศ์เธอพระองค์เจ้า', 'Inactive'),
(20, '107', 'สมเด็จพระราชชนนี', 'Inactive'),
(21, '108', 'สมเด็จพระเจ้าพี่นางเธอเจ้าฟ้า', 'Inactive'),
(22, '109', 'สมเด็จพระ', 'Inactive'),
(23, '110', 'สมเด็จพระเจ้าลูกเธอ', 'Inactive'),
(24, '111', 'สมเด็จพระเจ้าลูกยาเธอ', 'Inactive'),
(25, '112', 'สมเด็จเจ้าฟ้า', 'Inactive'),
(26, '113', 'พระเจ้าบรมวงศ์เธอ', 'Inactive'),
(27, '114', 'พระเจ้าวรวงศ์เธอ', 'Inactive'),
(28, '115', 'พระเจ้าหลานเธอ', 'Inactive'),
(29, '116', 'พระเจ้าหลานยาเธอ', 'Inactive'),
(30, '117', 'พระวรวงศ์เธอ', 'Inactive'),
(31, '118', 'สมเด็จพระเจ้าภคินีเธอ', 'Inactive'),
(32, '119', 'พระองค์เจ้า', 'Inactive'),
(33, '120', 'หม่อมเจ้า', 'Inactive'),
(34, '121', 'หม่อมราชวงศ์', 'Inactive'),
(35, '122', 'หม่อมหลวง', 'Inactive'),
(36, '123', 'พระยา', 'Inactive'),
(37, '124', 'หลวง', 'Inactive'),
(38, '125', 'ขุน', 'Inactive'),
(39, '126', 'หมื่น', 'Inactive'),
(40, '127', 'เจ้าคุณ', 'Inactive'),
(41, '128', 'พระวรวงศ์เธอพระองค์เจ้า', 'Inactive'),
(42, '129', 'คุณ', 'Inactive'),
(43, '130', 'คุณหญิง', 'Inactive'),
(44, '131', 'ท่านผู้หญิงหม่อมหลวง', 'Inactive'),
(45, '132', 'ศาสตราจารย์นายแพทย์', 'Inactive'),
(46, '133', 'แพทย์หญิงคุณหญิง', 'Inactive'),
(47, '134', 'นายแพทย์', 'Inactive'),
(48, '135', 'แพทย์หญิง', 'Inactive'),
(49, '136', 'ทัณตแพทย์', 'Inactive'),
(50, '137', 'ทัณตแพทย์หญิง', 'Inactive'),
(51, '138', 'สัตวแพทย์', 'Inactive'),
(52, '139', 'สัตวแพทย์หญิง', 'Inactive'),
(53, '140', 'ดอกเตอร์', 'Inactive'),
(54, '141', 'ผู้ช่วยศาสตราจารย์', 'Inactive'),
(55, '142', 'รองศาสตราจารย์', 'Inactive'),
(56, '143', 'ศาสตราจารย์', 'Inactive'),
(57, '144', 'เภสัชกรชาย', 'Inactive'),
(58, '145', 'เภสัชกรหญิง', 'Inactive'),
(59, '146', 'หม่อม', 'Inactive'),
(60, '147', 'รองอำมาตย์เอก', 'Inactive'),
(61, '148', 'ท้าว', 'Inactive'),
(62, '149', 'เจ้า', 'Inactive'),
(63, '150', 'ท่านผู้หญิง', 'Inactive'),
(64, '151', 'คุณพระ', 'Inactive'),
(65, '152', 'ศาสตราจารย์คุณหญิง', 'Inactive'),
(66, '153', 'ซิสเตอร์', 'Inactive'),
(67, '154', 'เจ้าชาย', 'Inactive'),
(68, '155', 'เจ้าหญิง', 'Inactive'),
(69, '156', 'รองเสวกตรี', 'Inactive'),
(70, '157', 'เสด็จเจ้า', 'Inactive'),
(71, '158', 'เอกอัครราชฑูต', 'Inactive'),
(72, '159', 'พลสารวัตร', 'Inactive'),
(73, '160', 'สมเด็จเจ้า', 'Inactive'),
(74, '161', 'เจ้าฟ้า', 'Inactive'),
(75, '162', 'รองอำมาตย์ตรี', 'Inactive'),
(76, '163', 'หม่อมเจ้าหญิง', 'Inactive'),
(77, '164', 'ทูลกระหม่อม', 'Inactive'),
(78, '165', 'ศาสตราจารย์ดอกเตอร์', 'Inactive'),
(79, '166', 'เจ้านาง', 'Inactive'),
(80, '167', 'จ่าสำรอง', 'Inactive'),
(81, '200', 'พลเอก', 'Inactive'),
(82, '201', 'ว่าที่พลเอก', 'Inactive'),
(83, '202', 'พลโท', 'Inactive'),
(84, '204', 'พลตรี', 'Inactive'),
(85, '205', 'ว่าที่พลตรี', 'Inactive'),
(86, '206', 'พันเอกพิเศษ', 'Inactive'),
(87, '207', 'ว่าที่พันเอกพิเศษ', 'Inactive'),
(88, '208', 'พันเอก', 'Inactive'),
(89, '209', 'ว่าที่พันเอก', 'Inactive'),
(90, '210', 'พันโท', 'Inactive'),
(91, '211', 'ว่าที่พันโท', 'Inactive'),
(92, '212', 'พันตรี', 'Inactive'),
(93, '213', 'ว่าที่พันตรี', 'Inactive'),
(94, '214', 'ร้อยเอก', 'Inactive'),
(95, '215', 'ว่าที่ร้อยเอก', 'Inactive'),
(96, '216', 'ร้อยโท', 'Inactive'),
(97, '217', 'ว่าที่ร้อยโท', 'Inactive'),
(98, '218', 'ร้อยตรี', 'Inactive'),
(99, '219', 'ว่าที่ร้อยตรี', 'Inactive'),
(100, '220', 'จ่าสิบเอก', 'Inactive'),
(101, '221', 'จ่าสิบโท', 'Inactive'),
(102, '222', 'จ่าสิบตรี', 'Inactive'),
(103, '223', 'สิบเอก', 'Inactive'),
(104, '224', 'สิบโท', 'Inactive'),
(105, '225', 'สิบตรี', 'Inactive'),
(106, '226', 'พลทหาร', 'Inactive'),
(107, '227', 'นักเรียนนายร้อย', 'Inactive'),
(108, '228', 'นักเรียนนายสิบ', 'Inactive'),
(109, '229', 'พลจัตวา', 'Inactive'),
(110, '230', 'พลฯ อาสาสมัคร', 'Inactive'),
(111, '231', 'ร้อยเอกหม่อมเจ้า', 'Inactive'),
(112, '232', 'พลโทหม่อมเจ้า', 'Inactive'),
(113, '233', 'ร้อยตรีหม่อมเจ้า', 'Inactive'),
(114, '234', 'ร้อยโทหม่อมเจ้า', 'Inactive'),
(115, '235', 'พันโทหม่อมเจ้า', 'Inactive'),
(116, '236', 'พันเอกหม่อมเจ้า', 'Inactive'),
(117, '237', 'พันตรีหม่อมราชวงศ์', 'Inactive'),
(118, '238', 'พันโทหม่อมราชวงศ์', 'Inactive'),
(119, '239', 'สิบตรีหม่อมราชวงศ์', 'Inactive'),
(120, '240', 'พันเอกหม่อมราชวงศ์', 'Inactive'),
(121, '241', 'จ่าสิบเอกหม่อมราชวงศ์', 'Inactive'),
(122, '242', 'ร้อยเอกหม่อมราชวงศ์', 'Inactive'),
(123, '243', 'ร้อยตรีหม่อมราชวงศ์', 'Inactive'),
(124, '244', 'สิบเอกหม่อมราชวงศ์', 'Inactive'),
(125, '245', 'ร้อยโทหม่อมราชวงศ์', 'Inactive'),
(126, '246', 'พันเอก(พิเศษ)หม่อมราชวงศ์', 'Inactive'),
(127, '247', 'พลฯหม่อมหลวง', 'Inactive'),
(128, '248', 'ร้อยเอกหม่อมหลวง', 'Inactive'),
(129, '249', 'สิบโทหม่อมหลวง', 'Inactive'),
(130, '250', 'พลโทหม่อมหลวง', 'Inactive'),
(131, '251', 'ร้อยโทหม่อมหลวง', 'Inactive'),
(132, '252', 'ร้อยตรีหม่อมหลวง', 'Inactive'),
(133, '253', 'สิบเอกหม่อมหลวง', 'Inactive'),
(134, '254', 'พลตรีหม่อมหลวง', 'Inactive'),
(135, '255', 'พันตรีหม่อมหลวง', 'Inactive'),
(136, '256', 'พันเอกหม่อมหลวง', 'Inactive'),
(137, '257', 'พันโทหม่อมหลวง', 'Inactive'),
(138, '258', 'จ่าสิบตรีหม่อมหลวง', 'Inactive'),
(139, '259', 'นักเรียนนายร้อยหม่อมหลวง', 'Inactive'),
(140, '260', 'ว่าที่ร้อยตรีหม่อมหลวง', 'Inactive'),
(141, '261', 'จ่าสิบเอกหม่อมหลวง', 'Inactive'),
(142, '262', 'ร้อยเอกนายแพทย์', 'Inactive'),
(143, '263', 'ร้อยเอกแพทย์หญิง', 'Inactive'),
(144, '264', 'ร้อยโทนายแพทย์', 'Inactive'),
(145, '265', 'พันตรีนายแพทย์', 'Inactive'),
(146, '266', 'ว่าที่ร้อยโทนายแพทย์', 'Inactive'),
(147, '267', 'พันเอกนายแพทย์', 'Inactive'),
(148, '268', 'ร้อยตรีนายแพทย์', 'Inactive'),
(149, '269', 'ร้อยโทแพทย์หญิง', 'Inactive'),
(150, '270', 'พลตรีนายแพทย์', 'Inactive'),
(151, '271', 'พันโทนายแพทย์', 'Inactive'),
(152, '272', 'จอมพล', 'Inactive'),
(153, '273', 'พันโทหลวง', 'Inactive'),
(154, '274', 'พันตรีพระเจ้าวรวงศ์เธอพระองค์เจ้า', 'Inactive'),
(155, '275', 'ศาสตราจารย์พันเอก', 'Inactive'),
(156, '276', 'พันตรีหลวง', 'Inactive'),
(157, '277', 'พลโทหลวง', 'Inactive'),
(158, '278', 'ร้อยโทดอกเตอร์', 'Inactive'),
(159, '279', 'ร้อยเอกดอกเตอร์', 'Inactive'),
(160, '280', 'สารวัตรทหาร', 'Inactive'),
(161, '281', 'ร้อยตรีดอกเตอร์', 'Inactive'),
(162, '282', 'พันตรีคุณหญิง', 'Inactive'),
(163, '283', 'จ่าสิบตรีหม่อมราชวงศ์', 'Inactive'),
(164, '284', 'พลจัตวาหลวง', 'Inactive'),
(165, '285', 'พลตรีหม่อมราชวงศ์', 'Inactive'),
(166, '286', 'พันตรีพระเจ้าวรวงศ์เธอพระองค์', 'Inactive'),
(167, '287', 'ท่านผู้หญิงหม่อมราชวงศ์', 'Inactive'),
(168, '288', 'ศาสตราจารย์ร้อยเอก', 'Inactive'),
(169, '289', 'พันโทคุณหญิง', 'Inactive'),
(170, '290', 'ร้อยตรีแพทย์หญิง', 'Inactive'),
(171, '291', 'พลเอกหม่อมหลวง', 'Inactive'),
(172, '292', 'ว่าที่ร้อยตรีหม่อมราชวงศ์', 'Inactive'),
(173, '293', 'พันเอกหญิงคุณหญิง', 'Inactive'),
(174, '294', 'จ่าสิบเอกพิเศษ', 'Inactive'),
(175, '351', 'พลเรือเอก', 'Inactive'),
(176, '352', 'ว่าที่พลเรือเอก', 'Inactive'),
(177, '353', 'พลเรือโท', 'Inactive'),
(178, '354', 'ว่าที่พลเรือโท', 'Inactive'),
(179, '355', 'พลเรือตรี', 'Inactive'),
(180, '356', 'ว่าที่พลเรือตรี', 'Inactive'),
(181, '357', 'นาวาเอกพิเศษ', 'Inactive'),
(182, '358', 'ว่าที่นาวาเอกพิเศษ', 'Inactive'),
(183, '359', 'นาวาเอก', 'Inactive'),
(184, '360', 'ว่าที่นาวาเอก', 'Inactive'),
(185, '361', 'นาวาโท', 'Inactive'),
(186, '362', 'ว่าที่นาวาโท', 'Inactive'),
(187, '363', 'นาวาตรี', 'Inactive'),
(188, '364', 'ว่าที่นาวาตรี', 'Inactive'),
(189, '365', 'เรือเอก', 'Inactive'),
(190, '366', 'ว่าที่เรือเอก', 'Inactive'),
(191, '367', 'เรือโท', 'Inactive'),
(192, '368', 'ว่าที่เรือโท', 'Inactive'),
(193, '369', 'เรือตรี', 'Inactive'),
(194, '370', 'ว่าที่เรือตรี', 'Inactive'),
(195, '371', 'พันจ่าเอก', 'Inactive'),
(196, '372', 'พันจ่าโท', 'Inactive'),
(197, '373', 'พันจ่าตรี', 'Inactive'),
(198, '374', 'จ่าเอก', 'Inactive'),
(199, '375', 'จ่าโท', 'Inactive'),
(200, '376', 'จ่าตรี', 'Inactive'),
(201, '377', 'พลฯทหารเรือ', 'Inactive'),
(202, '378', 'นักเรียนนายเรือ', 'Inactive'),
(203, '379', 'นักเรียนจ่าทหารเรือ', 'Inactive'),
(204, '380', 'พลเรือจัตวา', 'Inactive'),
(205, '381', 'นาวาโทหม่อมเจ้า', 'Inactive'),
(206, '382', 'นาวาเอกหม่อมเจ้า', 'Inactive'),
(207, '383', 'นาวาตรีหม่อมเจ้า', 'Inactive'),
(208, '384', 'พลเรือตรีหม่อมราชวงศ์', 'Inactive'),
(209, '385', 'นาวาเอกหม่อมราชวงศ์', 'Inactive'),
(210, '386', 'นาวาโทหม่อมราชวงศ์', 'Inactive'),
(211, '387', 'นาวาตรีหม่อมราชวงศ์', 'Inactive'),
(212, '388', 'นาวาโทหม่อมหลวง', 'Inactive'),
(213, '389', 'นาวาตรีหม่อมหลวง', 'Inactive'),
(214, '390', 'พันจ่าเอกหม่อมหลวง', 'Inactive'),
(215, '391', 'นาวาตรีแพทย์หญิง', 'Inactive'),
(216, '392', 'นาวาอากาศเอกหลวง', 'Inactive'),
(217, '393', 'พลเรือตรีหม่อมเจ้า', 'Inactive'),
(218, '395', 'นาวาตรีนายแพทย์', 'Inactive'),
(219, '396', 'พลเรือตรีหม่อมหลวง', 'Inactive'),
(220, '500', 'พลอากาศเอก', 'Inactive'),
(221, '501', 'ว่าที่พลอากาศเอก', 'Inactive'),
(222, '502', 'พลอากาศโท', 'Inactive'),
(223, '503', 'ว่าที่พลอากาศโท', 'Inactive'),
(224, '504', 'พลอากาศตรี', 'Inactive'),
(225, '505', 'ว่าที่พลอากาศตรี', 'Inactive'),
(226, '506', 'นาวาอากาศเอกพิเศษ', 'Inactive'),
(227, '507', 'ว่าที่นาวาอากาศเอกพิเศษ', 'Inactive'),
(228, '508', 'นาวาอากาศเอก', 'Inactive'),
(229, '509', 'ว่าที่นาวาอากาศเอก', 'Inactive'),
(230, '510', 'นาวาอากาศโท', 'Inactive'),
(231, '511', 'ว่าที่นาวาอากาศโท', 'Inactive'),
(232, '512', 'นาวาอากาศตรี', 'Inactive'),
(233, '513', 'ว่าที่นาวาอากาศตรี', 'Inactive'),
(234, '514', 'เรืออากาศเอก', 'Inactive'),
(235, '515', 'ว่าที่เรืออากาศเอก', 'Inactive'),
(236, '516', 'เรืออากาศโท', 'Inactive'),
(237, '517', 'ว่าที่เรืออากาศโท', 'Inactive'),
(238, '518', 'เรืออากาศตรี', 'Inactive'),
(239, '519', 'ว่าที่เรืออากาศตรี', 'Inactive'),
(240, '520', 'พันจ่าอากาศเอก', 'Inactive'),
(241, '521', 'พันจ่าอากาศโท', 'Inactive'),
(242, '522', 'พันจ่าอากาศตรี', 'Inactive'),
(243, '523', 'จ่าอากาศเอก', 'Inactive'),
(244, '524', 'จ่าอากาศโท', 'Inactive'),
(245, '525', 'จ่าอากาศตรี', 'Inactive'),
(246, '526', 'พลฯทหารอากาศ', 'Inactive'),
(247, '527', 'นักเรียนนายเรืออากาศ', 'Inactive'),
(248, '528', 'นักเรียนจ่าอากาศ', 'Inactive'),
(249, '529', 'นักเรียนพยาบาลทหารอากาศ', 'Inactive'),
(250, '530', 'พันอากาศเอกหม่อมราชวงศ์', 'Inactive'),
(251, '531', 'พลอากาศตรีหม่อมราชวงศ์', 'Inactive'),
(252, '532', 'พลอากาศโทหม่อมหลวง', 'Inactive'),
(253, '533', 'เรืออากาศโทขุน', 'Inactive'),
(254, '534', 'พันจ่าอากาศเอกหม่อมหลวง', 'Inactive'),
(255, '535', 'เรืออากาศเอกนายแพทย์', 'Inactive'),
(256, '536', 'พลอากาศเอกหม่อมราชวงศ์', 'Inactive'),
(257, '537', 'พลอากาศตรีหม่อมหลวง', 'Inactive'),
(258, '538', 'พลอากาศจัตวา', 'Inactive'),
(259, '539', 'พลอากาศโทหม่อมราชวงศ์', 'Inactive'),
(260, '540', 'นาวาอากาศเอกหม่อมหลวง', 'Inactive'),
(261, '606', 'พระครูพิบูลสมณธรรม', 'Inactive'),
(262, '651', 'พลตำรวจเอก', 'Inactive'),
(263, '652', 'ว่าที่พลตำรวจเอก', 'Inactive'),
(264, '653', 'พลตำรวจโท', 'Inactive'),
(265, '654', 'ว่าที่พลตำรวจโท', 'Inactive'),
(266, '655', 'พลตำรวจตรี', 'Inactive'),
(267, '656', 'ว่าที่พลตำรวจตรี', 'Inactive'),
(268, '657', 'พลตำรวจจัตวา', 'Inactive'),
(269, '658', 'ว่าที่พลตำรวจจัตวา', 'Inactive'),
(270, '659', 'พันตำรวจเอก (พิเศษ)', 'Inactive'),
(271, '660', 'ว่าที่พันตำรวจเอก(พิเศษ)', 'Inactive'),
(272, '661', 'พันตำรวจเอก', 'Inactive'),
(273, '662', 'ว่าที่พันตำรวจเอก', 'Inactive'),
(274, '663', 'พันตำรวจโท', 'Inactive'),
(275, '664', 'ว่าที่พันตำรวจโท', 'Inactive'),
(276, '665', 'พันตำรวจตรี', 'Inactive'),
(277, '666', 'ว่าที่พันตำรวจตรี', 'Inactive'),
(278, '667', 'ร้อยตำรวจเอก', 'Inactive'),
(279, '668', 'ว่าที่ร้อยตำรวจเอก', 'Inactive'),
(280, '669', 'ร้อยตำรวจโท', 'Inactive'),
(281, '670', 'ว่าที่ร้อยตำรวจโท', 'Inactive'),
(282, '671', 'ร้อยตำรวจตรี', 'Inactive'),
(283, '672', 'ว่าที่ร้อยตำรวจตรี', 'Inactive'),
(284, '673', 'นายดาบตำรวจ', 'Inactive'),
(285, '674', 'จ่าสิบตำรวจ', 'Inactive'),
(286, '675', 'สิบตำรวจเอก', 'Inactive'),
(287, '676', 'สิบตำรวจโท', 'Inactive'),
(288, '677', 'สิบตำรวจตรี', 'Inactive'),
(289, '678', 'นักเรียนนายร้อยตำรวจ', 'Inactive'),
(290, '679', 'นักเรียนนายสิบตำรวจ', 'Inactive'),
(291, '680', 'นักเรียนพลตำรวจ', 'Inactive'),
(292, '681', 'พลตำรวจ', 'Inactive'),
(293, '682', 'พลตำรวจพิเศษ', 'Inactive'),
(294, '683', 'พลตำรวจอาสาสมัคร', 'Inactive'),
(295, '684', 'พลตำรวจสำรอง', 'Inactive'),
(296, '685', 'พลตำรวจสำรองพิเศษ', 'Inactive'),
(297, '686', 'พลตำรวจสมัคร', 'Inactive'),
(298, '687', 'สมาชิกอาสารักษาดินแดน', 'Inactive'),
(299, '688', 'นายกองใหญ่', 'Inactive'),
(300, '689', 'นายกองเอก', 'Inactive'),
(301, '690', 'นายกองโท', 'Inactive'),
(302, '691', 'นายกองตรี', 'Inactive'),
(303, '692', 'นายหมวดเอก', 'Inactive'),
(304, '693', 'นายหมวดโท', 'Inactive'),
(305, '694', 'นายหมวดตรี', 'Inactive'),
(306, '695', 'นายหมู่ใหญ่', 'Inactive'),
(307, '696', 'นายหมู่เอก', 'Inactive'),
(308, '697', 'นายหมู่โท', 'Inactive'),
(309, '698', 'นายหมู่ตรี', 'Inactive'),
(310, '699', 'สมาชิกเอก', 'Inactive'),
(311, '700', 'สมาชิกโท', 'Inactive'),
(312, '701', 'สมาชิกตรี', 'Inactive'),
(313, '702', 'อาสาสมัครทหารพราน', 'Inactive'),
(314, '703', 'พันตำรวจโทหม่อมเจ้า', 'Inactive'),
(315, '704', 'พันตำรวจเอกหม่อมเจ้า', 'Inactive'),
(316, '705', 'นักเรียนนายร้อยตำรวจหม่อมเจ้า', 'Inactive'),
(317, '706', 'พลตำรวจตรีหม่อมราชวงศ์', 'Inactive'),
(318, '707', 'พันตำรวจตรีหม่อมราชวงศ์', 'Inactive'),
(319, '708', 'พันตำรวจโทหม่อมราชวงศ์', 'Inactive'),
(320, '709', 'พันตำรวจเอกหม่อมราชวงศ์', 'Inactive'),
(321, '710', 'ร้อยตำรวจเอกหม่อมราชวงศ์', 'Inactive'),
(322, '711', 'สิบตำรวจเอกหม่อมหลวง', 'Inactive'),
(323, '712', 'พันตำรวจเอกหม่อมหลวง', 'Inactive'),
(324, '713', 'พันตำรวจโทหม่อมหลวง', 'Inactive'),
(325, '714', 'นักเรียนนายร้อยตำรวจหม่อมหลวง', 'Inactive'),
(326, '715', 'ร้อยตำรวจโทหม่อมหลวง', 'Inactive'),
(327, '716', 'นายดาบตำรวจหม่อมหลวง', 'Inactive'),
(328, '717', 'พันตำรวจตรีหม่อมหลวง', 'Inactive'),
(329, '718', 'ศาสตราจารย์นายแพทย์พันตำรวจเอก', 'Inactive'),
(330, '719', 'พันตำรวจโทนายแพทย์', 'Inactive'),
(331, '720', 'ร้อยตำรวจโทนายแพทย์', 'Inactive'),
(332, '721', 'ร้อยตำรวจเอกนายแพทย์', 'Inactive'),
(333, '722', 'พันตำรวจตรีนายแพทย์', 'Inactive'),
(334, '723', 'พันตำรวจเอกนายแพทย์', 'Inactive'),
(335, '724', 'พันตำรวจตรีหลวง', 'Inactive'),
(336, '725', 'ร้อยตำรวจโทดอกเตอร์', 'Inactive'),
(337, '726', 'พันตำรวจเอกดอกเตอร์', 'Inactive'),
(338, '727', 'ร้อยตำรวจเอกหม่อมหลวง', 'Inactive'),
(339, '729', 'พันตำรวจเอกหญิง ท่านผู้หญิง', 'Inactive'),
(340, '730', 'พลตำรวจตรีหม่อมหลวง', 'Inactive'),
(341, '731', 'พลตรีหญิง คุณหญิง', 'Inactive'),
(342, '732', 'ว่าที่สิบเอก', 'Inactive'),
(343, '733', 'พลตำรวจเอกดอกเตอร์', 'Inactive'),
(344, '800', 'สมเด็จพระสังฆราชเจ้า', 'Inactive'),
(345, '801', 'สมเด็จพระสังฆราช', 'Inactive'),
(346, '802', 'สมเด็จพระราชาคณะ', 'Inactive'),
(347, '803', 'รองสมเด็จพระราชาคณะ', 'Inactive'),
(348, '804', 'พระราชาคณะ', 'Inactive'),
(349, '805', 'พระเปรียญธรรม', 'Inactive'),
(350, '806', 'พระหิรัญยบัฏ', 'Inactive'),
(351, '807', 'พระสัญญาบัตร', 'Inactive'),
(352, '808', 'พระราช', 'Inactive'),
(353, '809', 'พระเทพ', 'Inactive'),
(354, '810', 'พระปลัดขวา', 'Inactive'),
(355, '811', 'พระปลัดซ้าย', 'Inactive'),
(356, '812', 'พระครูปลัด', 'Inactive'),
(357, '813', 'พระครูปลัดสุวัฒนญาณคุณ', 'Inactive'),
(358, '814', 'พระครูปลัดอาจารย์วัฒน์', 'Inactive'),
(359, '815', 'พระครูวิมลสิริวัฒน์', 'Inactive'),
(360, '816', 'พระสมุห์', 'Inactive'),
(361, '817', 'พระครูสมุห์', 'Inactive'),
(362, '818', 'พระครู', 'Inactive'),
(363, '819', 'พระครูใบฎีกา', 'Inactive'),
(364, '820', 'พระครูธรรมธร', 'Inactive'),
(365, '821', 'พระครูวิมลภาณ', 'Inactive'),
(366, '822', 'พระครูศัพทมงคล', 'Inactive'),
(367, '823', 'พระครูสังฆภารวิชัย', 'Inactive'),
(368, '824', 'พระครูสังฆรักษ์', 'Inactive'),
(369, '825', 'พระครูสังฆวิชัย', 'Inactive'),
(370, '826', 'พระครูสังฆวิชิต', 'Inactive'),
(371, '827', 'พระปิฎก', 'Inactive'),
(372, '828', 'พระปริยัติ', 'Inactive'),
(373, '829', 'เจ้าอธิการ', 'Inactive'),
(374, '830', 'พระอธิการ', 'Inactive'),
(375, '831', 'พระ', 'Inactive'),
(376, '832', 'สามเณร', 'Inactive'),
(377, '833', 'พระปลัด', 'Inactive'),
(378, '834', 'สมเด็จพระอริยวงศาคตญาณ', 'Inactive'),
(379, '835', 'พระคาร์ดินัล', 'Inactive'),
(380, '836', 'พระสังฆราช', 'Inactive'),
(381, '837', 'พระคุณเจ้า', 'Inactive'),
(382, '838', 'บาทหลวง', 'Inactive'),
(383, '839', 'พระมหา', 'Inactive'),
(384, '840', 'พระราชปัญญา', 'Inactive'),
(385, '841', 'ภาราดา', 'Inactive'),
(386, '842', 'พระศรีปริยัติธาดา', 'Inactive'),
(387, '843', 'พระญาณโศภณ', 'Inactive'),
(388, '844', 'สมเด็จพระมหาวีรวงศ์', 'Inactive'),
(389, '845', 'พระโสภณธรรมาภรณ์', 'Inactive'),
(390, '846', 'พระวิริวัฒน์วิสุทธิ์', 'Inactive'),
(391, '847', 'พระญาณ', 'Inactive'),
(392, '848', 'พระอัครสังฆราช', 'Inactive'),
(393, '849', 'พระธรรม', 'Inactive'),
(394, '850', 'พระสาสนโสภณ', 'Inactive'),
(395, '851', 'พระธรรมโสภณ', 'Inactive'),
(396, '852', 'พระอุดมสารโสภณ', 'Inactive'),
(397, '853', 'พระครูวิมลญาณโสภณ', 'Inactive'),
(398, '854', 'พระครูปัญญาภรณโสภณ', 'Inactive'),
(399, '855', 'พระครูโสภณปริยัติคุณ', 'Inactive'),
(400, '856', 'พระอธิธรรม', 'Inactive'),
(401, '857', 'พระราชญาณ', 'Inactive'),
(402, '858', 'พระสุธีวัชโรดม', 'Inactive'),
(403, '859', 'รองเจ้าอธิการ', 'Inactive'),
(404, '860', 'พระครูวินัยธร', 'Inactive'),
(405, '861', 'พระศรีวชิราภรณ์', 'Inactive'),
(406, '862', 'พระราชบัณฑิต', 'Inactive'),
(407, '863', 'แม่ชี', 'Inactive'),
(408, '864', 'นักบวช', 'Inactive'),
(409, '865', 'พระรัตน', 'Inactive'),
(410, '866', 'พระโสภณปริยัติธรรม', 'Inactive'),
(411, '867', 'พระครูวิศาลปัญญาคุณ', 'Inactive'),
(412, '868', 'พระศรีปริยัติโมลี', 'Inactive'),
(413, '869', 'พระครูวัชรสีลาภรณ์', 'Inactive'),
(414, '870', 'พระครูพิพัฒน์บรรณกิจ', 'Inactive'),
(415, '871', 'พระครูวิบูลธรรมกิจ', 'Inactive'),
(416, '872', 'พระครูพัฒนสารคุณ', 'Inactive'),
(417, '873', 'พระครูสุวรรณพัฒนคุณ', 'Inactive'),
(418, '874', 'พระครูพรหมวีรสุนทร', 'Inactive'),
(419, '875', 'พระครูอุปถัมภ์นันทกิจ', 'Inactive'),
(420, '876', 'พระครูวิจารณ์สังฆกิจ', 'Inactive'),
(421, '877', 'พระครูวิมลสารวิสุทธิ์', 'Inactive'),
(422, '878', 'พระครูไพศาลศุภกิจ', 'Inactive'),
(423, '879', 'พระครูโอภาสธรรมพิมล', 'Inactive'),
(424, '880', 'พระครูพิพิธวรคุณ', 'Inactive'),
(425, '881', 'พระครูสุนทรปภากร', 'Inactive'),
(426, '882', 'พระครูสิริชัยสถิต', 'Inactive'),
(427, '883', 'พระครูเกษมธรรมานันท์', 'Inactive'),
(428, '884', 'พระครูถาวรสันติคุณ', 'Inactive'),
(429, '885', 'พระครูวิสุทธาจารวิมล', 'Inactive'),
(430, '886', 'พระครูปภัสสราธิคุณ', 'Inactive'),
(431, '887', 'พระครูวรสังฆกิจ', 'Inactive'),
(432, '888', 'พระครูไพบูลชัยสิทธิ์', 'Inactive'),
(433, '889', 'พระครูโกวิทธรรมโสภณ', 'Inactive'),
(434, '890', 'พระครูสุพจน์วราภรณ์', 'Inactive'),
(435, '891', 'พระครูไพโรจน์อริญชัย', 'Inactive'),
(436, '892', 'พระครูสุนทรคณาภิรักษ์', 'Inactive'),
(437, '893', 'พระสรภาณโกศล', 'Inactive'),
(438, '894', 'พระครูประโชติธรรมรัตน์', 'Inactive'),
(439, '895', 'พระครูจารุธรรมกิตติ์', 'Inactive'),
(440, '896', 'พระครูพิทักษ์พรหมรังษี', 'Inactive'),
(441, '897', 'พระศรีปริยัติบัณฑิต', 'Inactive'),
(442, '898', 'พระครูพุทธิธรรมานุศาสน์', 'Inactive'),
(443, '899', 'พระธรรมเมธาจารย์', 'Inactive'),
(444, '900', 'พระครูกิตติกาญจนวงศ์', 'Inactive'),
(445, '901', 'พระครูปลัดสัมพิพัฒนวิริยาจารย์', 'Inactive'),
(446, '902', 'พระครูศีลกันตาภรณ์', 'Inactive'),
(447, '903', 'พระครูประกาศพุทธพากย์', 'Inactive'),
(448, '904', 'พระครูอมรวิสุทธิคุณ', 'Inactive'),
(449, '905', 'พระครูสุทัศน์ธรรมาภิรม', 'Inactive'),
(450, '906', 'พระครูอุปถัมภ์วชิโรภาส', 'Inactive'),
(451, '907', 'พระครูสุนทรสมณคุณ', 'Inactive'),
(452, '908', 'พระพรหมมุนี', 'Inactive'),
(453, '909', 'พระครูสิริคุณารักษ์', 'Inactive'),
(454, '910', 'พระครูวิชิตพัฒนคุณ', 'Inactive'),
(455, '911', 'พระครูพิบูลโชติธรรม', 'Inactive'),
(456, '912', 'พระพิศาลสารคุณ', 'Inactive'),
(457, '913', 'พระรัตนมงคลวิสุทธ์', 'Inactive'),
(458, '914', 'พระครูโสภณคุณานุกูล', 'Inactive'),
(459, '915', 'พระครูผาสุกวิหารการ', 'Inactive'),
(460, '916', 'พระครูวชิรวุฒิกร', 'Inactive'),
(461, '917', 'พระครูกาญจนยติกิจ', 'Inactive'),
(462, '918', 'พระครูบวรรัตนวงศ์', 'Inactive'),
(463, '919', 'พระราชพัชราภรณ์', 'Inactive'),
(464, '920', 'พระครูพิพิธอุดมคุณ', 'Inactive'),
(465, '921', 'องสุตบทบวร', 'Inactive'),
(466, '922', 'พระครูจันทเขมคุณ', 'Inactive'),
(467, '923', 'พระครูศีลสารวิสุทธิ์', 'Inactive'),
(468, '924', 'พระครูสุธรรมโสภิต', 'Inactive'),
(469, '925', 'พระครูอุเทศธรรมนิวิฐ', 'Inactive'),
(470, '926', 'พระครูบรรณวัตร', 'Inactive'),
(471, '927', 'พระครูวิสุทธาจาร', 'Inactive'),
(472, '928', 'พระครูสุนทรวรวัฒน์', 'Inactive'),
(473, '929', 'พระเทพชลธารมุนี ศรีชลบุราจารย์', 'Inactive'),
(474, '930', 'พระครูโสภณสมุทรคุณ', 'Inactive'),
(475, '931', 'พระราชเมธาภรณ์', 'Inactive'),
(476, '932', 'พระครูศรัทธาธรรมโสภณ', 'Inactive'),
(477, '933', 'พระครูสังฆบริรักษ์', 'Inactive'),
(478, '934', 'พระมหานายก', 'Inactive'),
(479, '935', 'พระครูโอภาสสมาจาร', 'Inactive'),
(480, '936', 'พระครูศรีธวัชคุณาภรณ์', 'Inactive'),
(481, '937', 'พระครูโสภิตวัชรกิจ', 'Inactive'),
(482, '938', 'พระราชวชิราภรณ์', 'Inactive'),
(483, '939', 'พระครูสุนทรวรธัช', 'Inactive'),
(484, '940', 'พระครูอาทรโพธิกิจ', 'Inactive'),
(485, '941', 'พระครูวิบูลกาญจนกิจ', 'Inactive'),
(486, '942', 'พระพรหมวชิรญาณ', 'Inactive'),
(487, '943', 'พระครูสุพจน์วรคุณ', 'Inactive'),
(488, '944', 'พระราชาวิมลโมลี', 'Inactive'),
(489, '945', 'พระครูอมรธรรมนายก', 'Inactive'),
(490, '946', 'พระครูพิศิษฎ์ศาสนการ', 'Inactive'),
(491, '947', 'พระครูเมธีธรรมานุยุต', 'Inactive'),
(492, '948', 'พระครูปิยสีลสาร', 'Inactive'),
(493, '949', 'พระครูสถิตบุญวัฒน์', 'Inactive'),
(494, '950', 'พระครูนิเทศปิยธรรม', 'Inactive'),
(495, '951', 'พระครูวิสุทธิ์กิจจานุกูล', 'Inactive'),
(496, '952', 'พระครูสถิตย์บุญวัฒน์', 'Inactive'),
(497, '953', 'พระครูประโชติธรรมานุกูล', 'Inactive'),
(498, '954', 'พระเทพญาณกวี', 'Inactive'),
(499, '955', 'พระครูพิพัฒน์ชินวงศ์', 'Inactive'),
(500, '956', 'พระครูสมุทรขันตยาภรณ์', 'Inactive'),
(501, '957', 'พระครูภาวนาวรกิจ', 'Inactive'),
(502, '958', 'พระครูศรีศาสนคุณ', 'Inactive'),
(503, '959', 'พระครูวิบูลย์ธรรมศาสก์', 'Inactive');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_courses`
-- (See below for the actual view)
--
CREATE TABLE `view_courses` (
`courses_id` varchar(20)
,`name` varchar(150)
,`description` varchar(250)
,`category` int(10)
,`subject` varchar(250)
,`starttime` time
,`endtime` time
,`number_student` int(5)
,`number_student_registered` int(10)
,`category_name` varchar(150)
,`category_name_th` varchar(150)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_memberships`
-- (See below for the actual view)
--
CREATE TABLE `view_memberships` (
`id` bigint(20)
,`username` varchar(250)
,`password` varchar(64)
,`prename` int(5)
,`firstname` varchar(255)
,`lastname` varchar(255)
,`nickname` varchar(50)
,`gender` int(5)
,`birthday` date
,`mobile` varchar(255)
,`email` varchar(255)
,`type` int(5)
,`image` varchar(250)
,`facebook` varchar(150)
,`line` varchar(150)
,`view` tinyint(1)
,`edit` tinyint(1)
,`del` tinyint(1)
,`token` varchar(50)
,`insert_user_id` varchar(250)
,`insert_datetime` datetime
,`update_user_id` varchar(250)
,`update_datetime` timestamp
,`gender_name` varchar(150)
,`gender_name_th` varchar(150)
,`type_name` varchar(150)
,`type_name_th` varchar(150)
,`prename_th` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `view_courses`
--
DROP TABLE IF EXISTS `view_courses`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_courses`  AS  select `courses`.`courses_id` AS `courses_id`,`courses`.`name` AS `name`,`courses`.`description` AS `description`,`courses`.`category` AS `category`,`courses`.`subject` AS `subject`,`courses`.`starttime` AS `starttime`,`courses`.`endtime` AS `endtime`,`courses`.`number_student` AS `number_student`,`courses`.`number_student_registered` AS `number_student_registered`,`courses_category`.`name` AS `category_name`,`courses_category`.`name_th` AS `category_name_th` from (`courses` join `courses_category`) where `courses`.`category` = `courses_category`.`category_id` ;

-- --------------------------------------------------------

--
-- Structure for view `view_memberships`
--
DROP TABLE IF EXISTS `view_memberships`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_memberships`  AS  select `memberships`.`id` AS `id`,`memberships`.`username` AS `username`,`memberships`.`password` AS `password`,`memberships`.`prename` AS `prename`,`memberships`.`firstname` AS `firstname`,`memberships`.`lastname` AS `lastname`,`memberships`.`nickname` AS `nickname`,`memberships`.`gender` AS `gender`,`memberships`.`birthday` AS `birthday`,`memberships`.`mobile` AS `mobile`,`memberships`.`email` AS `email`,`memberships`.`type` AS `type`,`memberships`.`image` AS `image`,`memberships`.`facebook` AS `facebook`,`memberships`.`line` AS `line`,`memberships`.`view` AS `view`,`memberships`.`edit` AS `edit`,`memberships`.`del` AS `del`,`memberships`.`token` AS `token`,`memberships`.`insert_user_id` AS `insert_user_id`,`memberships`.`insert_datetime` AS `insert_datetime`,`memberships`.`update_user_id` AS `update_user_id`,`memberships`.`update_datetime` AS `update_datetime`,`membership_gender`.`name` AS `gender_name`,`membership_gender`.`name_th` AS `gender_name_th`,`membership_type`.`name` AS `type_name`,`membership_type`.`name_th` AS `type_name_th`,`prename`.`prename_th` AS `prename_th` from (((`memberships` join `membership_gender`) join `membership_type`) join `prename`) where `memberships`.`gender` = `membership_gender`.`type_id` and `memberships`.`type` = `membership_type`.`type_id` and `memberships`.`prename` = `prename`.`pren_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`courses_id`),
  ADD KEY `FK_Category` (`category`);

--
-- Indexes for table `courses_category`
--
ALTER TABLE `courses_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `courses_dayofweek`
--
ALTER TABLE `courses_dayofweek`
  ADD PRIMARY KEY (`day_id`);

--
-- Indexes for table `memberships`
--
ALTER TABLE `memberships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Category` (`prename`),
  ADD KEY `FK_Gender` (`gender`),
  ADD KEY `FK_Type` (`type`);

--
-- Indexes for table `membership_gender`
--
ALTER TABLE `membership_gender`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `membership_type`
--
ALTER TABLE `membership_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `prename`
--
ALTER TABLE `prename`
  ADD PRIMARY KEY (`pren_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses_category`
--
ALTER TABLE `courses_category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `courses_dayofweek`
--
ALTER TABLE `courses_dayofweek`
  MODIFY `day_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `memberships`
--
ALTER TABLE `memberships`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'user id', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `membership_gender`
--
ALTER TABLE `membership_gender`
  MODIFY `type_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `membership_type`
--
ALTER TABLE `membership_type`
  MODIFY `type_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `memberships`
--
ALTER TABLE `memberships`
  ADD CONSTRAINT `FK_Category` FOREIGN KEY (`prename`) REFERENCES `prename` (`pren_id`),
  ADD CONSTRAINT `FK_Gender` FOREIGN KEY (`gender`) REFERENCES `membership_gender` (`type_id`),
  ADD CONSTRAINT `FK_Type` FOREIGN KEY (`type`) REFERENCES `membership_type` (`type_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
