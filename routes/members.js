var express = require('express');
var router = express.Router();
var User = require("../models/Members");

/* GET users listing. */
router.get('/', getAll);
router.get('/types',getalltype);
router.get('/genders',getallgender);
router.get('/prenameother', getPrenameOther);
router.get('/prenameactive', getPrenameActive);
router.get('/username/:username?', getUserByUsername);
router.get('/:id?', getById);
router.post('/authenticate', authenticate);
router.post('/add', addUser);
router.put('/update/:id?', updateUser);
router.delete('/delete/:id?', deleteUser);

function authenticate(req, res, next) {

  User.authenticate(req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {

      res.json(rows);
    }
  })
}

function getAll(req, res, next) {
  User.getAllUser(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getPrenameActive(req, res, next) {
  User.getPrenameActive(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getPrenameOther(req, res, next) {
  User.getPrenameOther(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getUserByUsername(req, res, next) {
  User.getUserByUsername(req.params.username, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getById(req, res, next) {
  User.getById(req.params.id, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function addUser(req, res, next) {
  User.addUser(req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function updateUser(req, res, next) {
  User.updateUser(req.params.id, req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function deleteUser(req, res, next) {
  User.deleteUser(req.params.id, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}
function getalltype(req, res, next) {
  User.getalltype(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getallgender(req, res, next) {
  User.getAllGender(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}
module.exports = router;
