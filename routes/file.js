var express = require('express');
var _router = express.Router();
var multer = require('multer');
var path = require('path');
var fs = require('fs');

var store = multer.diskStorage({
    destination: function (req, file, cb) {
        let filepath = './public/images/' + req.body.filepath;
        if (!fs.existsSync(filepath)) {
            fs.mkdirSync(filepath);
        }
        cb(null, filepath);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
        // cb(null, Date.now()+"."+file.originalname);

    }
});


var upload = multer({ storage: store }).single('file');

_router.post('/upload', function (req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            return res.status(501).json({ error: err });
        }
        //do all database record saving activity
        return res.json({url:req.body.fileurl+req.body.filepath+"/"+req.file.filename, originalname: req.file.originalname, uploadname: req.file.filename });

    });
});


_router.post('/download', function (req, res, next) {
    filepath = path.join(__dirname, '../public/images') + '/' + req.body.filename;
    res.sendFile(filepath);


});

_router.post('/delete', function (req, res, next) {
    filepath = path.join(__dirname, '../public/images') + '/' + req.body.filepath;

    if (fs.existsSync(filepath)) {
        // Do something
        fs.unlinkSync(filepath);
        return res.status(200).json({ message: "delete file " + filepath + "Success!" });
    } else {
        return res.status(200).json({ message: "Not File!" });

    }
});

module.exports = _router;