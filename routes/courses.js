var express = require('express');
var router = express.Router();
var Courses = require("../models/Courses");

/* GET users listing. */
router.get('/', getAll);
router.get('/:course_id?', checkCourseID);
router.get('/categorys',getallcategorys);
router.get('/getalldays',getalldays);
router.post('/add', addCourse);
router.post('/check', checkCourse);
router.post('/checkname', checCoursekName);
router.post('/search', searchCourse);
router.put('/update/:course_id?', updateCourse);
router.delete('/delete/:course_id?', deleteCourse);

function getAll(req, res, next) {
  Courses.getAllCourses(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getallcategorys(req, res, next) {
  Courses.getallcategorys(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getalldays(req, res, next) {
  Courses.getalldays(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function addCourse(req, res, next) {
  Courses.addCourse(req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function checkCourse(req, res, next) {
  Courses.checkCourse(req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function checkCourseID(req, res, next) {
  Courses.checkCourseID(req.params.course_id, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function checCoursekName(req, res, next) {
  Courses.checCoursekName(req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function searchCourse(req, res, next) {
  Courses.searchCourse(req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function updateCourse(req, res, next) {
  Courses.updateCourse(req.params.course_id, req.body, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function deleteCourse(req, res, next) {
  Courses.deleteCourse(req.params.course_id, function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}
function getalltype(req, res, next) {
  User.getalltype(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}

function getallgender(req, res, next) {
  User.getAllGender(function (err, rows) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(rows);
    }
  });
}
module.exports = router;
