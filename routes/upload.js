var express = require('express');
var router = express.Router();
var Upload = require("../models/Upload");
var multer = require('multer');
var multerupload = multer({ dest: './asset/' })

router.post('/', multerupload.any(), Upload.fileupload);

module.exports = router;